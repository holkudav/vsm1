import java.io.File
import kotlin.math.log

fun main(args: Array<String>) {
    val probabilities = File(args[0])
        .readText(Charsets.UTF_8)
        .split("\n")[1]
        .charCount()
        .probabilities()

    println("Probabilities:\n{")
    probabilities.forEach {
        println("\t\"${it.char}\": ${it.probability},")
    }
    println("}\n")
    println("Entropy: ${entropy(probabilities)}\n")
    
    println("Optimal code:\n{")
    println(Huffman.calculate(probabilities))
    println("}\n")
}

fun entropy(probabilities: Collection<MyChar>): Double {
    return probabilities.fold(0.0, { acc, p ->
        acc - p.probability * log(p.probability, 2.0)
    })
}

private fun Map<String, Int>.probabilities(): List<MyChar> {
    val n = this.values.sum().toDouble()
    return this.map { MyChar(it.key, it.value / n) }
        .sortedBy { it.probability }
}

private fun String.charCount(): Map<String, Int> {
    return this.fold(emptyMap(), { acc, c ->
        val char = c.toString()
        if (acc.containsKey(char)) {
            acc + Pair(char, acc[char]!! +1)
        } else {
            acc + Pair(char, 1)
        }
    })
}

import java.util.Comparator
import java.util.PriorityQueue

data class MyChar (val char: String, val probability: Double) {
    operator fun plus(myChar: MyChar): MyChar {
        return MyChar(char + myChar.char, probability + myChar.probability)
    }
}

data class HuffmanNode (
    var probability: Double = 0.0,
    var c: String = "0",
    var left: HuffmanNode? = null,
    var right: HuffmanNode? = null,
) {

    fun getCodes() = codes(this, "")

    private fun codes(root: HuffmanNode?, s: String): String {
        if (root!!.left == null && root.right == null) {
            return "\"${root.c}\"\t| $s\n"
        }
        return codes(root.left, s + "0") + codes(root.right, s + "1")
    }
}

internal class ImplementComparator : Comparator<HuffmanNode> {
    override fun compare(x: HuffmanNode, y: HuffmanNode): Int {
        return when {
            x.probability >  y.probability -> 1
            x.probability <  y.probability -> -1
            else -> 0
        }
    }
} // IMplementing the huffman algorithm

object Huffman {


    fun calculate(chars: List<MyChar>): String {
        val q = PriorityQueue(chars.size, ImplementComparator())
        chars.forEach { q.add(HuffmanNode(it.probability, it.char)) }

        var root: HuffmanNode? = null
        while (q.size > 1) {
            val x = q.poll()
            val y = q.poll()
            val f = HuffmanNode(x.probability + y.probability, "-", x, y)
            root = f
            q.add(f)
        }
        return root!!.getCodes();
    }
}